import React from "react"
import styled from "styled-components"

import { StaticImage } from "gatsby-plugin-image"
import Layout from "../components/layout"
import SeoData from "../components/SeoData"
import { H1, H2, H3, P } from "../components/elements"

import MaxWidthSection from "../components/layout/MaxWidthSection"
import FullWidthSection from "../components/layout/FullWidthSection"
import Column from "../components/layout/Column"

import FeaturesSection from "../portfolio/datocms-modular-page-building/features-section.mp4"
import PageSettings from "../portfolio/datocms-modular-page-building/page-settings.mp4"
import SectionSettings from "../portfolio/datocms-modular-page-building/section-settings.mp4"
import BackgroundStyle from "../portfolio/datocms-modular-page-building/background-style.mp4"
import OneColumnGeneralContent from "../portfolio/datocms-modular-page-building/one-column-general-content.mp4"

const FeatureImage = styled(FullWidthSection)`
  .gatsby-image-wrapper {
    max-height: 600px;
  }
`

const DatoCmsModularPage = () => {
  return (
    <Layout>
      <SeoData title="Modular Web Page Building with DatoCMS" />
      <MaxWidthSection as="header">
        <Column>
          <H1>Modular Web Page Building with DatoCMS</H1>
        </Column>
      </MaxWidthSection>
      <FeatureImage>
        <Column>
          <StaticImage
            src="../portfolio/datocms-modular-page-building/feature-image.png"
            alt="content model diagram"
            placeholder="blurred"
            layout="constrained"
          />
        </Column>
      </FeatureImage>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <P>
            Using a CMS framework can facilitate the creation of live web pages
            by users without any technical developer skills. At 1VALET, we were
            looking to replace printed brochures with a digital web page for new
            customers. The purpose was to allow our customer experience team to
            easily and quickly create a new web page brochure for new customers.
          </P>
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <H2>Leveraging DatoCMS's Modular Content Features</H2>
          <video
            muted
            loop
            autoPlay
            style={{ width: "100%", marginBottom: "2rem" }}
          >
            <source src={OneColumnGeneralContent} type="video/mp4" />
          </video>
          <P>
            One of DatoCMS's most rich feature is the modular content field that
            allows users to add content dynamically to pages. By leveraging this
            feature, we were able to create pre-defined sections/layouts for our
            pages that content created can choose from when creating a page.
          </P>
          <H3>
            Which page building strategy is best? Adding rows and columns, or
            pre-defined content blocks?
          </H3>
          <P>
            This was on of the the first questions I had to answer for myself
            when creating the content model for the project. Considering our
            users will be non-technical users and marketing content creators,
            what would the page building experience be if they were presentation
            with actional buttons labbelled "Add Row" and "Add Column", compared
            to a more descriptive / pre-defined option like "Add Feature
            Section". If I try to put myself in the shoes on the users, and have
            a specific layout in mind I want to add to a page, am I more likely
            to say "Ok, let me go ahead and add a row section, and then then 3
            columns, one for each feature I want.", or would I be more likely to
            say "Ok, let me go ahead and add a features section"? I would argue
            that the latter is more likely to be aligned with the way they
            outline page content.
          </P>
          <P>
            To differentiate between different types of content and sections
            mith multiple columns, I grouped different layouts intro One or Two
            Columns Section, followed by the content type they represent, eg.:
            General Content, Features or Step by Step Process.{" "}
          </P>
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <H2>Pre-templated content for better site-wide content management</H2>
          <video
            muted
            loop
            autoPlay
            style={{ width: "100%", marginBottom: "2rem" }}
          >
            <source src={FeaturesSection} type="video/mp4" />
          </video>
          <P>
            A huge advantage to content modelling is the ability to creating
            relationships between different peices of content, to better help
            management content in the event of changes and updates. If a piece
            of content, such as a feature, is displayed in multiple pages of
            your website, a CMS that enables you to create a interdependent
            relationship between that features' content, and the pages it's
            present in, allows to you update that feature in a centralized
            setting. The changes then propagate site-wide, creating a better
            user experience compared to the "headache" of manually updating the
            different instances of that features across the website.
          </P>
          <P>
            This strategy was best put to use by creating a separate record of
            each feature. Upon adding a Feature Section to a page, users are
            able to select which features apply to each customers. Through the
            API, we're then able to fetch the related data/content for each
            feature in the front-end.
          </P>
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <H2>Page and Section specific settings</H2>
          <video
            muted
            loop
            autoPlay
            style={{ width: "100%", marginBottom: "2rem" }}
          >
            <source src={PageSettings} type="video/mp4" />
          </video>
          <P>
            Even when creating pages that follow templated layouts and
            structures, I consider it to be good practice to include the
            possibliity to customize data. This is helpful in the event of
            needing to include page-specific scripts or target specific page
            sections through ID's. The page content model includes the ability
            to add a page settings module for users or developers in need of
            adding meta data such as page ID's, classes, custom head and footer
            scripts, etc.
          </P>
          <video
            muted
            loop
            autoPlay
            style={{ width: "100%", marginBottom: "2rem" }}
          >
            <source src={SectionSettings} type="video/mp4" />
          </video>
          <P>
            Likewise, sections should also include some form of custom data
            handling. Each section includes a section specific meta data blocs.
            This creates an oppportunity for anchors links to scroll to a
            specific section ID, or manually assign a content position.
          </P>
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <H2>Background styling</H2>
          <video
            muted
            loop
            autoPlay
            style={{ width: "100%", marginBottom: "2rem" }}
          >
            <source src={BackgroundStyle} type="video/mp4" />
          </video>
          <P>
            Similarly to the above meta data section, sections should also
            account for background settings.{" "}
            <em>
              Wouldn't a web page be boring if there were no background colors
              or images? I agree!
            </em>{" "}
            Each modular section can also have different backgrounds with an
            image and a tint overlay, or a simple color or gradient.
          </P>
        </Column>
      </MaxWidthSection>
    </Layout>
  )
}

export default DatoCmsModularPage
