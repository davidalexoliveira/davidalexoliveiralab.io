import React from "react"
import styled from "styled-components"
import { Helmet } from "react-helmet"

import Layout from "../components/layout"
import SeoData from "../components/SeoData"
import MaxWidthSection from "../components/layout/MaxWidthSection"
import Column from "../components/layout/Column"
import { Link } from "gatsby"

import { H1, P, A } from "../components/elements"

const LargeH1 = styled(H1)`
  font-size: 6rem;
`

const LargeP = styled(P)`
  font-size: 3rem;
  line-height: 120%;
`

const Highlight = styled.span`
  color: ${props => props.theme.colors.primary};
`

const HelloSplunk = () => {
  return (
    <Layout>
      <Helmet>
        <meta name={`robots`} content={`noindex, nofollow`} />
      </Helmet>
      <SeoData title="Hello Splunk" />
      <MaxWidthSection as="header">
        <Column>
          <LargeH1>Hello Splunk &#128075;</LargeH1>
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column>
          <LargeP>
            My name is David Oliveira and I'm a front-end designer/developer and{" "}
            <Highlight>
              I would like to express my interest in applying for the role of
              Front End Software Engineer at Splunk!
            </Highlight>
          </LargeP>
          <P>
            I'm a multidisciplinary designer / front-end developer with relevant
            experience in both areas. I’ve collaborated on both branding and
            rebranding initiatives as a visual designer, as well as lead the
            design, development, and maintenance of web experiences. The
            holistic nature of my experiences bring benefits when collaborating
            on multi-faceted projects that demand both design and online/web
            aspects. Aside from that, and my experience running a design
            business, I have a strong interest in web architectures,
            specifically headless. I think scalable and maintainable web
            frameworks are a must for teams who want to perform faster and
            achieve reliable consistency across their products and web. That's
            why I've taken proactive steps in the past couple of years to
            further develop my skills in{" "}
            <b>
              component-based frameworks, graphql, content modeling and atomic
              design systems
            </b>
            .
          </P>
          <P>
            I’d love the opportunity to learn more about the position and the
            contributions I can make to the team. My email is{" "}
            <A href="mailto:davidalex.oliveira@gmail.com">
              davidalex.oliveira@gmail.com
            </A>{" "}
            and you can find more information{" "}
            <A>
              <Link to="/about">about me</Link>
            </A>
            ,{" "}
            <A>
              <Link to="/work">my work</Link>
            </A>
            , or{" "}
            <A>
              <Link to="/resume">my resume</Link>
            </A>{" "}
            here on my website.
          </P>
          <P>Hope to hear from you soon!</P>
        </Column>
      </MaxWidthSection>
    </Layout>
  )
}

export default HelloSplunk
