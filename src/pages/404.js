import React from "react"

import Layout from "../components/layout"
import SeoData from "../components/SeoData"
import { Link } from "gatsby"

import { H1, P, A } from "../components/elements"

import MaxWidthSection from "../components/layout/MaxWidthSection"
import Column from "../components/layout/Column"

const NotFoundPage = () => (
  <Layout>
    <SeoData title="404: Not found" />
    <MaxWidthSection as="header">
      <Column lgStart={1} lgEnd={13}>
        <H1>404: Not Found</H1>
        <P>
          Use the following links if you're looking for more information{" "}
          <A>
            <Link to="/about">about me</Link>
          </A>
          ,{" "}
          <A>
            <Link to="/work">my work</Link>
          </A>
          , or{" "}
          <A>
            <Link to="/resume">my resume</Link>
          </A>{" "}
          here on my website.
        </P>
      </Column>
    </MaxWidthSection>
  </Layout>
)

export default NotFoundPage
