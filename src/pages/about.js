import React from "react"
import styled from "styled-components"

import { StaticImage } from "gatsby-plugin-image"
import Layout from "../components/layout"
import SeoData from "../components/SeoData"
import { H3, P, A } from "../components/elements"
import Figure from "../components/elements/Figure"

import MaxWidthSection from "../components/layout/MaxWidthSection"
import Column from "../components/layout/Column"

import lunaVideo from "../images/luna.mp4"

const ContentWrapper = styled.div`
  .gatsby-image-wrapper {
    max-height: 400px;
    margin-bottom: 2rem;
  }
`

const Wrapper = styled.div`
  margin-bottom: 4rem;
`

const About = () => {
  return (
    <Layout>
      <SeoData title="About me" />
      <MaxWidthSection as="header">
        <Column mdStart={3} mdEnd={11}>
          <ContentWrapper>
            <Wrapper>
              <StaticImage
                src="../images/forest.png"
                alt="David Oliveira"
                placeholder="blurred"
                layout="fullWidth"
              />
              <P>
                I've worked as an individual contributor collaborating in a
                cross-departmental team in the tech industry and as a business
                owner supervising a team of designers and producing work for
                overseas clients. These experiences have made me a
                multidisciplinary designer with experience in both graphic and
                web design, and given me invaluable knowledge in running a
                business and team.
              </P>
              <P>
                While living in Portugal, I started off as an enthusiast of SaaS
                online products, particularly Prezi. My participation in several
                early Prezi contests led me to becoming one of the first
                officially endorsed Prezi Experts back in 2012. From there, the
                business grew into a small team of four. Designing, managing
                workloads and communicating with clients became my day-to-day
                responsabilities for almost 6 years.
              </P>
              <P>
                Although I felt fortunate to have had such fulfilling and rich
                experiences early on in my career, I sought more development as
                a designer and decided to move to Canada and seek opportunities
                to embrace design roles within teams.
              </P>
            </Wrapper>
            <Wrapper>
              <Figure figcaption="Village of Nazaré, by the Atlantic Ocean">
                <StaticImage
                  src="../images/nazare.jpg"
                  alt="village by the ocean"
                  height=""
                  placeholder="blurred"
                  layout="fullWidth"
                />
              </Figure>
              <H3>Some say I'm crazy...</H3>
              <P>
                ...crazy to have picked Canada's chilling weather (especially
                Winnipeg...brrrrr <span style={{ color: "#0066ff" }}>❄</span>)
                to Portugal's warm and mediterranean climate. I definitely
                prefer temperatures above 0°C, but the opportunity to start a
                new chapter in my life was a challenge worth taking.
              </P>
            </Wrapper>
            <Wrapper>
              <H3>So...what am I up to today?</H3>
              <P>
                Nowadays I value authenticity and simplicity in my work and in
                my personal life. Currently, I live in Winnipeg with my dog{" "}
                <A href={lunaVideo} target="_blank">
                  Luna
                </A>{" "}
                and for the past 3 years, I've developed an interest in design
                systems, modern frameworks like React, and Jamstack
                architectures, and have actively sought to learn more about
                building modular design systems and solutions for web.
              </P>
              <P>
                <A
                  href="https://www.linkedin.com/in/davidalex-oliveira/"
                  target="_blank"
                >
                  Let's connect on LinkedIn if you'd like to talk more about my
                  work!
                </A>
              </P>
            </Wrapper>
          </ContentWrapper>
        </Column>
      </MaxWidthSection>
    </Layout>
  )
}

export default About
