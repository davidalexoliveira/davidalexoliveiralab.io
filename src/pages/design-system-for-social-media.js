import React from "react"
import styled from "styled-components"

import { StaticImage } from "gatsby-plugin-image"
import Layout from "../components/layout"
import SeoData from "../components/SeoData"
import { H1, H2, P, UList, ListItem } from "../components/elements"

import MaxWidthSection from "../components/layout/MaxWidthSection"
import FullWidthSection from "../components/layout/FullWidthSection"
import DisplayCase from "../components/layout/DisplayCase"
import Column from "../components/layout/Column"

import ComponentSets from "../portfolio/design-system-for-social-media/component-sets.mp4"

const FeatureImage = styled(FullWidthSection)`
  .gatsby-image-wrapper {
    max-height: 600px;
  }
`

const DesignSystemPage = () => {
  return (
    <Layout>
      <SeoData title="Design System for Social Media" />
      <MaxWidthSection as="header">
        <Column>
          <H1>Design System for Social Media</H1>
        </Column>
      </MaxWidthSection>
      <FeatureImage>
        <Column>
          <StaticImage
            src="../portfolio/design-system-for-social-media/feature-image.png"
            alt="social media graphic mockups"
            placeholder="blurred"
            layout="constrained"
          />
        </Column>
      </FeatureImage>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <P>
            With the HR teams' increased need for graphics for social media, I
            proposed to build a design system using Figma where the HR
            specialists would be able to export their own deliverables for
            Instagram and other social platforms. While empowering the HR
            specialists to be able produce graphics based on their needs, we
            were able to simultaneously free up time on the design team to focus
            and prioritize other projects in the pipeline.
          </P>
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <H2>Modular and interchangeable system</H2>
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/design-system-for-social-media/modular.png"
            alt="job posting social graphics template"
            placeholder="blurred"
            layout="constrained"
          />
          <P>
            The design system is composed of a colour pallete, illustrations and
            multiple sets of components for multiple types of social posts:
          </P>
          <UList>
            <ListItem>job postings</ListItem>
            <ListItem>new hire welcome posts</ListItem>
            <ListItem>community engagement posts</ListItem>
            <ListItem>generic content posts</ListItem>
          </UList>
          <P>
            With the use of Figma Component Sets, I was able to provide the team
            with a solution that takes HR just a few clicks to select the size,
            colors and illustrations they want to use for their social posts.
          </P>
          <video muted loop autoPlay style={{ width: "100%" }}>
            <source src={ComponentSets} type="video/mp4" />
          </video>
        </Column>
      </MaxWidthSection>
      <DisplayCase>
        <Column mdStart={2} mdEnd={6}>
          <StaticImage
            src="../portfolio/design-system-for-social-media/hr-colors.png"
            alt="color palette components"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            src="../portfolio/design-system-for-social-media/hr-generic.png"
            alt="generic social graphics"
            placeholder="blurred"
            layout="constrained"
          />
        </Column>
        <Column mdStart={6} mdEnd={10}>
          <StaticImage
            src="../portfolio/design-system-for-social-media/hr-illustrations.png"
            alt="social graphic illustrations"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            src="../portfolio/design-system-for-social-media/hr-advice.png"
            alt="social graphics career advice"
            placeholder="blurred"
            layout="constrained"
          />
        </Column>
        <Column mdStart={10} mdEnd={14}>
          <StaticImage
            src="../portfolio/design-system-for-social-media/hr-welcome.png"
            alt="social graphics welcome photos"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            src="../portfolio/design-system-for-social-media/hr-postings-single.png"
            alt="job posting social graphics template"
            placeholder="blurred"
            layout="constrained"
          />
        </Column>
      </DisplayCase>
    </Layout>
  )
}

export default DesignSystemPage
