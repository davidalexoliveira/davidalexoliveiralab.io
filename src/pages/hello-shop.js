import React from "react"
import styled from "styled-components"
import { Helmet } from "react-helmet"

import Layout from "../components/layout"
import SeoData from "../components/SeoData"
import MaxWidthSection from "../components/layout/MaxWidthSection"
import Column from "../components/layout/Column"
import { Link } from "gatsby"

import { H1, P, A } from "../components/elements"

const LargeH1 = styled(H1)`
  font-size: 6rem;
`

const LargeP = styled(P)`
  font-size: 3rem;
  line-height: 120%;
`

const Highlight = styled.span`
  color: ${props => props.theme.colors.primary};
`

const HelloShop = () => {
  return (
    <Layout>
      <Helmet>
        <meta name={`robots`} content={`noindex, nofollow`} />
      </Helmet>
      <SeoData title="Hello Shop" />
      <MaxWidthSection as="header">
        <Column>
          <LargeH1>Hello Shop &#128075;</LargeH1>
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column>
          <LargeP>
            My name is David Oliveira and I'm front end developer and{" "}
            <Highlight>
              I would like to express my interest in applying for the role of
              Senior Front End Engineer at Shop's team!
            </Highlight>
          </LargeP>
          <P>
            I couldn't just send my resume without a chance to introduce myself.
            I'm a multidisciplinary individual with relevant experience in
            design and front end development. I'm familiar with Shopify's work
            and products from my time at Bold Commerce (one of Shopify's top
            partners) and I'm an avid Jamstack enthusiast with knowledge in
            building front end experiences with headless CMS's and React/Gatsby.
          </P>
          <P>
            I would love to make time to speak with your team and show my
            excitement for your product and the tools Shopify has been building.
            My email is{" "}
            <A href="mailto:davidalex.oliveira@gmail.com">
              davidalex.oliveira@gmail.com
            </A>{" "}
            and you can find more information{" "}
            <A>
              <Link to="/about">about me</Link>
            </A>
            ,{" "}
            <A>
              <Link to="/work">my work</Link>
            </A>
            , or{" "}
            <A>
              <Link to="/resume">my resume</Link>
            </A>{" "}
            here on my website.
          </P>
          <P>Hope to hear from you soon!</P>
        </Column>
      </MaxWidthSection>
    </Layout>
  )
}

export default HelloShop
