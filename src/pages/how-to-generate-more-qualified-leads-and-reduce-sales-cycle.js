import React from "react"
import styled from "styled-components"

import { StaticImage } from "gatsby-plugin-image"
import Layout from "../components/layout"
import SeoData from "../components/SeoData"
import { H1, H2, H3, UList, ListItem, P, A } from "../components/elements"

import MaxWidthSection from "../components/layout/MaxWidthSection"
import FullWidthSection from "../components/layout/FullWidthSection"
import Column from "../components/layout/Column"

import MultiStepForm from "../portfolio/how-to-generate-more-qualified-leads-and-reduce-sales-cycle/breadcrumbs-form.mp4"

const FeatureImage = styled(FullWidthSection)`
  .gatsby-image-wrapper {
    max-height: 600px;
  }
`

const HowToGenerateMoreQualifiedLeadsAndReduceSalesCyclePage = () => {
  return (
    <Layout>
      <SeoData
        title="How to generate more qualified leads and reduce sales cycle up to
            90%"
      />
      <MaxWidthSection as="header">
        <Column>
          <H1>
            How to generate more qualified leads and reduce sales cycle up to
            90%
          </H1>
        </Column>
      </MaxWidthSection>
      <FeatureImage>
        <Column>
          <StaticImage
            src="../portfolio/how-to-generate-more-qualified-leads-and-reduce-sales-cycle/feature-image.png"
            alt="step by step form breakdown"
            placeholder="blurred"
            layout="constrained"
          />
        </Column>
      </FeatureImage>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <P>
            As any marketing team should, one of 1VALET's main marketing goals
            was to increase conversion rates through their website forms and
            generate better quality leads for their sales funnel.{" "}
            <b>
              However, generating more qualified leads requires gathering more
              information from a visitor. The more form fields you ask the user
              to fill, the more likely they are to abandon filling out the form.
            </b>{" "}
            Fortunately, you can improve usability, and therefore increase form
            submissions, by adopting a multi-step format that outperforms
            single-step forms.
          </P>
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <H2>Benefits of multi-step form</H2>
          <video
            muted
            loop
            autoPlay
            style={{ width: "100%", marginBottom: "2rem" }}
          >
            <source src={MultiStepForm} type="video/mp4" />
          </video>
          <P>
            A multi-step form breaks down a long-form multiple steps to make the
            experience less intimidating for users to fill out. This is a
            similar approach to multi-step checkout forms in the ecommerce
            industry - spacing out the information you need from users into
            different steps (billing, payment, order review) rather than
            presenting it all at once! Some of the benefits include:
          </P>
          <H3>01. Reduced psychological friction</H3>
          <P>
            Breaking down a longer form into multiple pieces can make the
            experience less intimidating.
          </P>
          <H3>02. A stronger signal of committment from the user</H3>
          <P>
            A user that fills out a multi-step form not only show committment
            and interest towards the offer, the more likely they are to be a
            qualified lead.
          </P>
          <H3>
            03. Leveraging the{" "}
            <A
              href="https://uxdesign.cc/designing-for-motivation-with-the-endowed-progress-effect-d213d5dd86a2"
              target="_blank"
              rel="noopener noreferrer"
            >
              endowed progress effect
            </A>
          </H3>
          <P>
            The endowed progress effect represents a cognitive bias in which
            people are more likely to commit to achieving a goal when they feel
            they have made progress toward it. In the case of filling out a
            form, as users progress through the different steps in the form the
            more likely they are to reach the end of the form and convert.
          </P>
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <H2>Splitting form field into separate categories</H2>
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/how-to-generate-more-qualified-leads-and-reduce-sales-cycle/multi-step-form.png"
            alt="single long form versus multi-step form"
            placeholder="blurred"
            layout="constrained"
          />
          <P>
            By separating individual fields from a longer form into separate
            related categories, you benefit from reducing the perceived
            committment of filling out a long form, as well as strategically
            requesting low-risk information from the user in the first steps
            such as their name or company, and ask for more valuable information
            such as email or annual revenue in later steps.
          </P>
          <UList>
            <ListItem>
              <b>Step 01:</b> First and last name
              <i> - common and easy request</i>
            </ListItem>
            <ListItem>
              <b>Step 02:</b> Company name
              <i> - not so hard of a request either</i>
            </ListItem>
            <ListItem>
              <b>Step 03:</b> City<i> - more specific than previous fields</i>
            </ListItem>
            <ListItem>
              <b>Step 04:</b> Quantity<i> - more specific details</i>
            </ListItem>
            <ListItem>
              <b>Step 05:</b> Email
              <i>
                {" "}
                - final step to seal the deal once all other information was
                given
              </i>
            </ListItem>
          </UList>
          <P>
            <b>
              With this multi-step form on 1valet.com, the team was able to
              generate more qualified leads for the sales funnel compared to a
              single long form, as well as reduce the sales cycle up to 90%.
            </b>
          </P>
        </Column>
      </MaxWidthSection>
    </Layout>
  )
}

export default HowToGenerateMoreQualifiedLeadsAndReduceSalesCyclePage
