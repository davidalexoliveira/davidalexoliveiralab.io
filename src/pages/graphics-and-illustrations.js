import React from "react"

import { StaticImage } from "gatsby-plugin-image"
import Layout from "../components/layout"
import SeoData from "../components/SeoData"
import { H1, H2, P } from "../components/elements"

import MaxWidthSection from "../components/layout/MaxWidthSection"
import Column from "../components/layout/Column"

import Version2Gif from "../portfolio/illustrations-and-graphics/versiontwo.gif"

const IllustrationsGraphicsPage = () => {
  return (
    <Layout>
      <SeoData title="Graphics and Illustrations" />
      <MaxWidthSection as="header">
        <Column mdStart={3} mdEnd={11}>
          <H1>Graphics and Illustrations</H1>
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <H2>Ecommerce Product Features</H2>
          <P>
            Collection of visuals that mix photography and illustration to
            showcase product development and storefront user interfaces.
          </P>
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/graphic-bold-1.jpg"
            alt="man surrounded by user interface and products"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/graphic-bold-2.jpg"
            alt="man surrounded by user interface and products"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/graphic-bold-3.jpg"
            alt="two people looking at a user interface"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/graphic-bold-4.jpg"
            alt="man looking at user interface"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/graphic-ui-1.png"
            alt="Checkout user interface with multicurrency"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/graphic-ui-2.png"
            alt="Product user interface for subscribing to a face cream"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/graphic-ui-3.png"
            alt="Product user interface upselling a coffee mug"
            placeholder="blurred"
            layout="constrained"
          />
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <H2>Custom illustrations</H2>
          <P>
            Highlight detailed custom illustrations designed to represent signs
            of wellness and empowerment.
          </P>
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/prezi-05.jpg"
            alt="Illustration of a male head sideways with a band-aid"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/prezi-06.jpg"
            alt="Illustration of a female head sideways with wings"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/prezi-07.jpg"
            alt="Illustration of a monk and monkey heads"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/prezi-08.jpg"
            alt="Illustration of a monk and monkey heads over the words Harmony Empowered State"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/prezi-09.jpg"
            alt="Illustration of two male heads thinking"
            placeholder="blurred"
            layout="constrained"
          />
          <img
            src={Version2Gif}
            alt="Diagram about emotions and behaviours"
            style={{ maxWidth: "100%" }}
          />
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <H2>Featured Customer Carousel</H2>
          <P>
            Image carousel design featuring subscription customers. The webpage
            images slide into a browser illustration as a visual metaphor for
            viewing the pages on a browser. Left and right slider buttons sync
            up with the customers logos at the bottom for easy access to any
            customer. Designed in Figma.
          </P>
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/illustration-gc500.jpg"
            alt="futuristic illustration of a city"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/illustration-headless.png"
            alt="isometric futuristic grid with a central pillar"
            placeholder="blurred"
            layout="constrained"
          />
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <H2>Event celebration images for social media</H2>
          <P>
            Collection of graphics shared on social media to celebrate annual
            events such as Halloween and Canada Day. The concepts introduces the
            brand logo as a seamless elements that co-exists within the graphic.
          </P>
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/social-bold-1.jpg"
            alt="social media graphics"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/social-bold-2.jpg"
            alt="social media graphics"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/social-bold-3.jpg"
            alt="social media graphics"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/social-bold-4.jpg"
            alt="social media graphics"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/social-bold-5.jpg"
            alt="social media graphics"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/social-bold-6.jpg"
            alt="social media graphics"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/social-bold-7.jpg"
            alt="social media graphics"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/social-bold-8.jpg"
            alt="social media graphics"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/social-bold-9.jpg"
            alt="social media graphics"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/social-bold-10.jpg"
            alt="social media graphics"
            placeholder="blurred"
            layout="constrained"
          />

          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/social-bold-11.jpg"
            alt="social media graphics"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/social-bold-12.jpg"
            alt="social media graphics"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/social-bold-13.jpg"
            alt="social media graphics"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/social-bold-14.jpg"
            alt="social media graphics"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/social-bold-15.jpg"
            alt="social media graphics"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/social-bold-16.jpg"
            alt="social media graphics"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/illustrations-and-graphics/social-bold-17.jpg"
            alt="social media graphics"
            placeholder="blurred"
            layout="constrained"
          />
        </Column>
      </MaxWidthSection>
    </Layout>
  )
}

export default IllustrationsGraphicsPage
