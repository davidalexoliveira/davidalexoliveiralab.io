import React from "react"
import styled from "styled-components"
import { Helmet } from "react-helmet"

import Layout from "../components/layout"
import SeoData from "../components/SeoData"
import MaxWidthSection from "../components/layout/MaxWidthSection"
import Column from "../components/layout/Column"
import { Link } from "gatsby"

import { H1, P, A } from "../components/elements"

const LargeH1 = styled(H1)`
  font-size: 6rem;
`

const LargeP = styled(P)`
  font-size: 3rem;
  line-height: 120%;
`

const Highlight = styled.span`
  color: ${props => props.theme.colors.primary};
`

const HelloBottomless = () => {
  return (
    <Layout>
      <Helmet>
        <meta name={`robots`} content={`noindex, nofollow`} />
      </Helmet>
      <SeoData title="Hello Bottomless" />
      <MaxWidthSection as="header">
        <Column>
          <LargeH1>Hello Bottomless &#128075;</LargeH1>
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column>
          <LargeP>
            My name is David Oliveira and I'm a front-end web designer and
            developer and{" "}
            <Highlight>
              I would love to apply to the role of Senior Web Designer on your
              team!
            </Highlight>
          </LargeP>
          <P>
            I couldn't just send my resume without a chance to introduce myself.
            I'm a multidisciplinary designer / developer with relevant
            experience in both areas. For the past 4 years, I've been leading
            the design, development, and maintenance of the web presences at
            Bold Commerce and 1VALET. Aside from that, and my experience running
            a design business, I have a strong interest in web architectures,
            specifically headless / Jamstack for websites. I think scalable and
            maintainable web frameworks are crucial if a marketing team wants to
            be more agile and achieve reliable consistency across their online
            presence. That's why I've taken proactive steps in the past couple
            of years to further develop my skills in{" "}
            <b>React, Graphql, content modeling and atomic design systems</b>.
          </P>
          <P>
            I would love to make time to speak with your team and show my
            excitement for your product and the tools Bottomless has been
            building. My email is{" "}
            <A href="mailto:davidalex.oliveira@gmail.com">
              davidalex.oliveira@gmail.com
            </A>{" "}
            and you can find more information{" "}
            <A>
              <Link to="/about">about me</Link>
            </A>
            ,{" "}
            <A>
              <Link to="/work">my work</Link>
            </A>
            , or{" "}
            <A>
              <Link to="/resume">my resume</Link>
            </A>{" "}
            here on my website.
          </P>
          <P>Hope to hear from you soon!</P>
        </Column>
      </MaxWidthSection>
    </Layout>
  )
}

export default HelloBottomless
