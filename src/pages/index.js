import React from "react"

import { Helmet } from "react-helmet"

import Layout from "../components/layout"
import Hero from "../components/Hero"

import SeoData from "../components/SeoData"

const IndexPage = () => {
  return (
    <Layout noNav noFooter>
      <Helmet />
      <SeoData />
      <Hero />
    </Layout>
  )
}

export default IndexPage
