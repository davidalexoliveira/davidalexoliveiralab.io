import React from "react"
import { Helmet } from "react-helmet"
import { StaticImage } from "gatsby-plugin-image"
import styled from "styled-components"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SeoData from "../components/SeoData"
import { H2 } from "../components/elements"
import Button from "../components/elements/Button"
import MaxWidthSection from "../components/layout/MaxWidthSection"
import Column from "../components/layout/Column"

const ListingProject = styled(MaxWidthSection)`
  grid-column: 1 / -1;

  .gatsby-image-wrapper {
    height: 100%;
    transition: transform 0.8s ease-in-out;
  }

  @media ${props => props.theme.breakpoints.md} {
    padding: 2rem;
    grid-column: 2 / 14;

    :hover {
      background: #fff;
      cursor: pointer;

      .gatsby-image-wrapper {
        transform: scale(1.02);
      }

      a {
        background-color: #000;
      }
    }
  }
`

const SmallListingProject = styled(Link)`
  padding: 1rem;
  display: block;

  :hover {
    background: #fff;

    a {
      background-color: #000;
    }
  }

  @media ${props => props.theme.breakpoints.md} {
    padding: 2rem;
  }
`

const SmallListingProjectWrapper = styled(MaxWidthSection)`
  grid-column: 1 / -1;

  @media ${props => props.theme.breakpoints.md} {
    grid-column: 2 / 14;
  }
`

const ImageColumn = styled(Column)`
  overflow: hidden;
`

const ContentColumn = styled(Column)`
  padding: 0 1rem;
  @media ${props => props.theme.breakpoints.md} {
    padding: 1rem;
  }
`

const Work = () => {
  return (
    <Layout>
      <Helmet />
      <SeoData />
      <ListingProject
        as={Link}
        href="/how-to-generate-more-qualified-leads-and-reduce-sales-cycle"
      >
        <ImageColumn mdStart={1} mdEnd={10}>
          <StaticImage
            src="../portfolio/how-to-generate-more-qualified-leads-and-reduce-sales-cycle/feature-image.png"
            alt="step by step form breakdown"
            placeholder="blurred"
            layout="constrained"
          />
        </ImageColumn>
        <ContentColumn mdStart={10} mdEnd={14}>
          <H2>
            How to generate more qualified leads and reduce sales cycle up to
            90%
          </H2>
          <Button>Tell me more!</Button>
        </ContentColumn>
      </ListingProject>
      <ListingProject as={Link} href="/datocms-modular-page-building">
        <ImageColumn mdStart={1} mdEnd={10}>
          <StaticImage
            src="../portfolio/datocms-modular-page-building/feature-image.png"
            alt="content model diagram"
            placeholder="blurred"
            layout="constrained"
          />
        </ImageColumn>
        <ContentColumn mdStart={10} mdEnd={14}>
          <H2>Modular Web Page Building with DatoCMS</H2>
          <Button>Tell me more!</Button>
        </ContentColumn>
      </ListingProject>
      <ListingProject as={Link} href="/headless-content-modeling-in-sanity">
        <ImageColumn mdStart={1} mdEnd={10}>
          <StaticImage
            src="../portfolio/headless-content-modeling-in-sanity/feature-image.png"
            alt="content model diagram"
            placeholder="blurred"
            layout="constrained"
          />
        </ImageColumn>
        <ContentColumn mdStart={10} mdEnd={14}>
          <H2>Headless Content Modeling (in)Sanity</H2>
          <Button>Tell me more!</Button>
        </ContentColumn>
      </ListingProject>
      <ListingProject as={Link} href="/design-system-for-social-media">
        <ImageColumn mdStart={1} mdEnd={10}>
          <StaticImage
            src="../portfolio/design-system-for-social-media/feature-image.png"
            alt="social media graphic mockups"
            placeholder="blurred"
            layout="constrained"
          />
        </ImageColumn>
        <ContentColumn mdStart={10} mdEnd={14}>
          <H2>Design System for Social Media</H2>
          <Button>Tell me more!</Button>
        </ContentColumn>
      </ListingProject>
      <ListingProject as={Link} href="/ecommerce-illustration-sets">
        <ImageColumn mdStart={1} mdEnd={10}>
          <StaticImage
            src="../portfolio/ecommerce-illustration-sets/feature-image.png"
            alt="icon building pattern"
            placeholder="blurred"
            layout="constrained"
          />
        </ImageColumn>
        <ContentColumn mdStart={10} mdEnd={14}>
          <H2>Ecommerce Illustration Sets</H2>
          <Button>Tell me more!</Button>
        </ContentColumn>
      </ListingProject>
      <SmallListingProjectWrapper>
        <Column mdStart={1} mdEnd={7}>
          <SmallListingProject to="/webdesign-and-landing-pages">
            <H2>Webdesign and Landing Pages</H2>
            <Button>Tell me more!</Button>
          </SmallListingProject>
        </Column>
        <Column mdStart={7} mdEnd={13}>
          <SmallListingProject to="/graphics-and-illustrations">
            <H2>Graphics and Illustrations</H2>
            <Button>Tell me more!</Button>
          </SmallListingProject>
        </Column>
      </SmallListingProjectWrapper>
    </Layout>
  )
}

export default Work
