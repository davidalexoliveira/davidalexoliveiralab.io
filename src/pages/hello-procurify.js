import React from "react"
import styled from "styled-components"
import { Helmet } from "react-helmet"

import Layout from "../components/layout"
import SeoData from "../components/SeoData"
import MaxWidthSection from "../components/layout/MaxWidthSection"
import Column from "../components/layout/Column"
import { Link } from "gatsby"

import { H1, P, A } from "../components/elements"

const LargeH1 = styled(H1)`
  font-size: 6rem;
`

const LargeP = styled(P)`
  font-size: 3rem;
  line-height: 120%;
`

const Highlight = styled.span`
  color: ${props => props.theme.colors.primary};
`

const HelloProcurify = () => {
  return (
    <Layout>
      <Helmet>
        <meta name={`robots`} content={`noindex, nofollow`} />
      </Helmet>
      <SeoData title="Hello Procufiry" />
      <MaxWidthSection as="header">
        <Column>
          <LargeH1>Hello Procufiry &#128075;</LargeH1>
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column>
          <LargeP>
            My name is David Oliveira and I'm a designer and frontend web
            developer and{" "}
            <Highlight>
              I would love to apply for the role of Senior Designer at
              Procufiry!
            </Highlight>
          </LargeP>
          <P>
            I'm a multidisciplinary designer and developer with relevant
            experience in both areas. For the past 4 years, I've been leading
            the brand design, development, and maintenance of the web presences
            at Bold Commerce and 1VALET's marketing teams. Along with my
            experience running a design business in the first half of my career,
            I have a strong interest in design systems and component-based web
            frameworks. I think scalable and maintainable web frameworks and
            modular design systems are crucial for teams who want to be more
            agile and achieve reliable consistency across their projects. That's
            why I've taken proactive steps in the past couple of years to
            further develop my skills in{" "}
            <b>
              component-based frameworks, content modeling and atomic design
              systems
            </b>
            .
          </P>
          <P>
            I’d love the opportunity to learn more about the position and the
            contributions I can make to the team. My email is{" "}
            <A href="mailto:davidalex.oliveira@gmail.com">
              davidalex.oliveira@gmail.com
            </A>{" "}
            and you can find more information{" "}
            <A>
              <Link to="/about">about me</Link>
            </A>
            ,{" "}
            <A>
              <Link to="/work">my work</Link>
            </A>
            , or{" "}
            <A>
              <Link to="/resume">my resume</Link>
            </A>{" "}
            here on my website.
          </P>
          <P>Hope to hear from you soon! </P>
        </Column>
      </MaxWidthSection>
    </Layout>
  )
}

export default HelloProcurify
