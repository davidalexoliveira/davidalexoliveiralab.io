import React from "react"
import styled from "styled-components"

import { StaticImage } from "gatsby-plugin-image"
import Layout from "../components/layout"
import SeoData from "../components/SeoData"
import { H1, H2, P, UList, ListItem } from "../components/elements"

import MaxWidthSection from "../components/layout/MaxWidthSection"
import FullWidthSection from "../components/layout/FullWidthSection"
import DisplayCase from "../components/layout/DisplayCase"
import Column from "../components/layout/Column"

const FeatureImage = styled(FullWidthSection)`
  .gatsby-image-wrapper {
    max-height: 600px;
  }
`

const FlexWrapperIllustrations = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;

  .gatsby-image-wrapper {
    height: auto;
    width: 815px;
  }
`

const FlexWrapperIcons = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;

  .gatsby-image-wrapper {
    height: auto;
    width: 267px;
    background: #fff;
  }
`

const EcommerceSetPage = () => {
  return (
    <Layout>
      <SeoData title="Ecommerce Illustration Sets" />
      <MaxWidthSection as="header">
        <Column>
          <H1>Ecommerce Illustration Sets</H1>
        </Column>
      </MaxWidthSection>
      <FeatureImage>
        <Column>
          <StaticImage
            src="../portfolio/ecommerce-illustration-sets/feature-image.png"
            alt="icon building pattern"
            placeholder="blurred"
            layout="constrained"
          />
        </Column>
      </FeatureImage>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <P>
            As part of a website redesign initiative and new product
            positioning, I created custom illustrations to represent our new
            product offering in ecommerce. Each of the 8 ecommerce solutions
            featured a custom isometric illustration themed after the solution.
            Additionally, a set of icons and graphics were designed as a modular
            system where different visuals could be put together to create
            different variations and theme icon illustrations.
          </P>
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <H2>Isometric illustrations</H2>
          <P>Each illustration was themed after an ecommerce solutions:</P>
          <UList>
            <ListItem>ecommerce</ListItem>
            <ListItem>multi-currency</ListItem>
            <ListItem>subscriptions</ListItem>
            <ListItem>checkout</ListItem>
            <ListItem>pricing</ListItem>
            <ListItem>B2B</ListItem>
            <ListItem>customer loyalty</ListItem>
            <ListItem>product customization</ListItem>
          </UList>
          <P>
            The illustrations were conceptualized to showcase ecommerce elements
            and visuals, while positioning reinforces their relationship and the
            story being told. Each illustration follows its own color pallete
            (previously assigned to each solution) and contains the company logo
            which represents the part the brand plays in each story.
          </P>
        </Column>
      </MaxWidthSection>
      <DisplayCase>
        <Column>
          <FlexWrapperIllustrations>
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-header-1.jpg"
              alt="Isometric illustration of a laptop and tablet"
              placeholder="blurred"
              layout="constrained"
            />
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-header-2.jpg"
              alt="Isometric illustration of a globe"
              placeholder="blurred"
              layout="constrained"
            />
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-header-3.jpg"
              alt="Isometric illustration of a credit card with laptop and subscription box"
              placeholder="blurred"
              layout="constrained"
            />
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-header-4.jpg"
              alt="Isometric illustration of a shopping cart and credit card payment"
              placeholder="blurred"
              layout="constrained"
            />
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-header-5.jpg"
              alt="Isometric illustration of charts and graphic"
              placeholder="blurred"
              layout="constrained"
            />
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-header-6.jpg"
              alt="Isometric illustration of business delivering product on a conveyor belt"
              placeholder="blurred"
              layout="constrained"
            />
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-header-7.jpg"
              alt="Isometric illustration of product bags and gifts"
              placeholder="blurred"
              layout="constrained"
            />
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-header-8.jpg"
              alt="Isometric illustration of a monitor showing product customization"
              placeholder="blurred"
              layout="constrained"
            />
          </FlexWrapperIllustrations>
        </Column>
      </DisplayCase>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <H2>Modular illustration library</H2>
          <P>
            Additionally, I was given full creative control to build an
            illustration set to be included in the website redesign. I
            conceptualized and created over 100 individual visuals that would
            represent different concepts in ecommerce and that could be paired
            with other visual elements to create larger illustrations sets. The
            illustrations were used on both marketing and appeared on our
            products for about 2 years.
          </P>
          <P>
            The main feature of the library was its modularity - visual could be
            put together independently by other designers in the company. Larger
            illustration sets followed a pattern of meaningful elements in their
            composition: a background line silhoutte shaped as a relevant icon
            (shield), a larger focus element (document/data), and additional
            smaller supporting visuals within the same theme (shield and lock
            for protection)
          </P>
          <StaticImage
            src="../portfolio/ecommerce-illustration-sets/feature-image.png"
            alt="icon building pattern"
            placeholder="blurred"
            layout="constrained"
          />
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <H2>Learnings</H2>
          <P>
            Although the illustration set was well adopted, I realized that it
            wasn’t scalable, and not having been built in a tool like Figma made
            it more difficult to manage all illustrations. The set quickly
            became too large, visual consistency began to dillute as more
            visuals were created and iterations made on existing graphics.
          </P>
        </Column>
      </MaxWidthSection>
      <DisplayCase>
        <Column>
          <FlexWrapperIcons>
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-1.png"
              alt="ecommerce icon illustration"
              placeholder="blurred"
              layout="constrained"
            />
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-2.png"
              alt="ecommerce icon illustration"
              placeholder="blurred"
              layout="constrained"
            />
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-3.png"
              alt="ecommerce icon illustration"
              placeholder="blurred"
              layout="constrained"
            />
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-4.png"
              alt="ecommerce icon illustration"
              placeholder="blurred"
              layout="constrained"
            />
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-5.png"
              alt="ecommerce icon illustration"
              placeholder="blurred"
              layout="constrained"
            />
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-6.png"
              alt="ecommerce icon illustration"
              placeholder="blurred"
              layout="constrained"
            />
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-7.png"
              alt="ecommerce icon illustration"
              placeholder="blurred"
              layout="constrained"
            />
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-8.png"
              alt="ecommerce icon illustration"
              placeholder="blurred"
              layout="constrained"
            />
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-9.png"
              alt="ecommerce icon illustration"
              placeholder="blurred"
              layout="constrained"
            />
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-10.png"
              alt="ecommerce icon illustration"
              placeholder="blurred"
              layout="constrained"
            />
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-11.png"
              alt="ecommerce icon illustration"
              placeholder="blurred"
              layout="constrained"
            />
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-12.png"
              alt="ecommerce icon illustration"
              placeholder="blurred"
              layout="constrained"
            />
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-13.png"
              alt="ecommerce icon illustration"
              placeholder="blurred"
              layout="constrained"
            />
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-14.png"
              alt="ecommerce icon illustration"
              placeholder="blurred"
              layout="constrained"
            />
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-15.png"
              alt="ecommerce icon illustration"
              placeholder="blurred"
              layout="constrained"
            />
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-16.png"
              alt="ecommerce icon illustration"
              placeholder="blurred"
              layout="constrained"
            />
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-17.png"
              alt="ecommerce icon illustration"
              placeholder="blurred"
              layout="constrained"
            />
            <StaticImage
              src="../portfolio/ecommerce-illustration-sets/illustration-18.png"
              alt="ecommerce icon illustration"
              placeholder="blurred"
              layout="constrained"
            />
          </FlexWrapperIcons>
        </Column>
      </DisplayCase>
    </Layout>
  )
}

export default EcommerceSetPage
