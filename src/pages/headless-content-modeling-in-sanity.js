import React from "react"
import styled from "styled-components"
import Prism from "prismjs"
import { useEffect } from "react"

import { StaticImage } from "gatsby-plugin-image"
import Layout from "../components/layout"
import SeoData from "../components/SeoData"
import { H1, H2, P, UList, ListItem } from "../components/elements"

import MaxWidthSection from "../components/layout/MaxWidthSection"
import FullWidthSection from "../components/layout/FullWidthSection"
import Column from "../components/layout/Column"

const FeatureImage = styled(FullWidthSection)`
  .gatsby-image-wrapper {
    max-height: 600px;
  }
`

const PreCode = styled.pre`
  margin-bottom: 2rem !important;

  code[class*="language-"],
  pre[class*="language-"] {
    white-space: pre-wrap;
  }
`

const podcastCode = `export default {
  name: 'podcastEpisode',
  title: 'Episode',
  type: 'document',
  fields: [
    {
      // object reference
      name: 'metaTitle',
      type: 'metaTitle'
    },
    {
      // object reference
      name: 'metaDescription',
      type: 'metaDescription'
    },
    {
      title: 'Guest',
      name: 'guest',
      type: 'reference',
      to: [{type: 'people'}]
    },

    // more field types
    ...

    {
      title: 'Transcript',
      type: 'array',
       of: [{type: 'block'}]
    },
  ]
}
`

const HeadlessSanityPage = () => {
  useEffect(() => {
    Prism.highlightAll()
  })
  return (
    <Layout>
      <SeoData title="Headless Content Modeling in Sanity" />
      <MaxWidthSection as="header">
        <Column>
          <H1>Headless Content Modeling (in)Sanity</H1>
        </Column>
      </MaxWidthSection>
      <FeatureImage>
        <Column>
          <StaticImage
            src="../portfolio/headless-content-modeling-in-sanity/feature-image.png"
            alt="content model diagram"
            placeholder="blurred"
            layout="constrained"
          />
        </Column>
      </FeatureImage>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <P>
            For about 4 years, the website at Bold Commerce was powered entirely
            by Jekyll, a static site generator. The tool served us well - we
            were able to deploy the necessarity content for our marketing
            initiatives. However, without a CMS connected to Jekyll, we were not
            only tasked with designing landing pages but also content entry,
            which would sometimes create a bottleneck situation that could be
            avoided if the marketeers were able to add the content and make
            edits to the copy themselves without the need to learn how to
            navigate the framework.
          </P>
          <P>
            As the team grew, the workflow became unsustainable and we realized
            that we need to find a solution that allowed our marketeers to edit
            and add content to the website, but also guarantee the web designers
            the flexibility of the tools they needed for a great developer
            experience.
          </P>
          <P>
            We suggested the re-development of our website using a Headless
            approach - decoupoling the content from the backend system. While
            still taking advantage of all the benefits of a static-site
            generator, we would pull content through APIs to populate the pages.
            This content would be housed in a separate tool, such as a CMS, for
            the marketing team to access and use.
          </P>
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <H2>React, Gatsby...and Sanity!</H2>
          <P>
            React was our framework of choice for its robustness, and we chose
            Gatsby for its page creation and routing features. As for a content
            entry framework, our candidates were Netlify, Prismic, and Sanity.
            Sanity quickly become the solution we were most interested in:
          </P>
          <UList>
            <ListItem>
              it treats content as data, instead of HTML elements
            </ListItem>
            <ListItem>editor is customizable, opinionated</ListItem>
            <ListItem>
              open-sourced tool with a growing community, and readily available
              resources and plugins
            </ListItem>
          </UList>
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <H2>First step: creating the content model</H2>
          <P>
            A content model maps out all the types of content and the
            relationships and dependencies between each other. When taking a
            headless approach to content, it's indispensable (and I mean it) to
            outline, preferably visually, all your content types. Using Figjam,
            we created a simple diagram to lay out our content types before
            building the schemas in Sanity. For example, all podcast episodes
            are composed of the following data:
          </P>
          <UList>
            <ListItem>title</ListItem>
            <ListItem>
              description (which can be used as a meta description for SEO
              purposes)
            </ListItem>
            <ListItem>podcast player</ListItem>
            <ListItem>episode duration</ListItem>
            <ListItem>guest</ListItem>
            <ListItem>transcript</ListItem>
          </UList>
          <P>
            Furthermore, a guest is also its own content type, and can include
            data such as:
          </P>
          <UList>
            <ListItem>name</ListItem>
            <ListItem>bio</ListItem>
            <ListItem>photo</ListItem>
            <ListItem>social profile links</ListItem>
          </UList>
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/headless-content-modeling-in-sanity/content-model.png"
            alt="diagram overview of content model"
            placeholder="blurred"
            layout="constrained"
          />
          <P>
            By preparing the content model before anything else, we get to learn
            how to best organize our content in order to build schemas in
            Sanity. If not done properly, this could create huge headaches in
            the future with data duplication.
          </P>
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <H2>Building Sanity schemas</H2>
          <P>
            Due to the size of the project, our team decided to tackle a smaller
            portion of our website - the podcast. While referencing the content
            model for the podcast, we began building the schemas in Sanity for
            the different content types on the podcast, while still leaving the
            structure scalable for the larger website in the future. An example
            of a schema for a podcast content type would be an episode:
          </P>
          <PreCode>
            <code className="language-javascript">{podcastCode}</code>
          </PreCode>
          <P>
            Similarly to React components, we approached schema building and
            structuring in a modular way, creating separate schemas types in
            separate files. If we ever need to make changes to all the metaTitle
            fields across our Sanity documents, we would only need to access one
            file.
          </P>
          <P>
            However, we found limitations with this approach. As we began to
            nest schemas type within other schema types, Sanity would render
            nested schemas type as collapsable elements once you've reached a
            third-level of nesting. To avoid giving our users (our marketing
            team) a poor editing experience, we avoided nesting beyond a
            third-level.
          </P>
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <H2>Future steps</H2>
          <P>
            Once all the schemas for the pocast were complete, we were able to
            begin developing the backend framework in React and Gastby and pull
            data from Sanity. The project should be complete by the end of Q3 in
            2022.
          </P>
        </Column>
      </MaxWidthSection>
    </Layout>
  )
}

export default HeadlessSanityPage
