import React from "react"
import styled from "styled-components"

import Layout from "../components/layout"
import SeoData from "../components/SeoData"
import MaxWidthSection from "../components/layout/MaxWidthSection"
import Column from "../components/layout/Column"
import DownloadBtn from "../components/DownloadBtn"
import {
  UList,
  ListItem,
  H2,
  H3,
  P,
  A,
  AnchorLink,
} from "../components/elements"

import downloadResume from "../downloads/DavidOliveira-Resume.pdf"

const ResumeNav = styled.nav`
  display: none;

  @media ${props => props.theme.breakpoints.lg} {
    display: flex;
    flex-direction: column;
    position: sticky;
    top: 20px;
  }
`

const Title = styled(H3)`
  color: ${props => props.theme.colors.primary};
`

const Subtitle = styled(H3)`
  color: ${props => props.theme.colors.primary};
  font-size: 1.25rem;
  margin-bottom: 1rem !important;
`

const Details = styled(P)`
  font-size: 1rem;
  line-height: 150%;
  color: #777;
  margin-bottom: 1rem;
`

const ResumeBlock = styled.div`
  margin-bottom: 4rem;

  > :last-child {
    margin-bottom: 0;
  }

  h3 {
    margin-bottom: 0;
  }

  @media ${props => props.theme.breakpoints.md} {
    display: flex;
    flex-direction: column;
  }
`

const ResumeFeaturesWrapper = styled.div`
  display: block;
`

const ResumeFeatures = styled.div`
  font-size: 1.125rem;
  line-height: 150%;
  margin-bottom: 2rem;

  h3 {
    margin-bottom: 1rem;
  }

  @media ${props => props.theme.breakpoints.md} {
    margin-bottom: 2rem;

    :last-child {
      margin-right: 0;
    }
  }
`

const DownloadMobile = styled.div`
  display: block;
  margin-bottom: 2rem;

  @media ${props => props.theme.breakpoints.lg} {
    display: none;
  }
`

const Resume = () => {
  return (
    <Layout>
      <SeoData title="Resume" />
      <MaxWidthSection>
        <Column lgStart={1} lgEnd={4}>
          <ResumeNav>
            <AnchorLink href="#work-experience" title="Work Experience">
              Work Experience
            </AnchorLink>
            <AnchorLink href="#education" title="Education">
              Education
            </AnchorLink>
            <AnchorLink href="#skills-and-tools" title="Skills and Tools">
              Skills and Tools
            </AnchorLink>
            <AnchorLink href="#achievements" title="Achievements">
              Achievements
            </AnchorLink>
            <DownloadBtn href={downloadResume} download>
              Download resume
            </DownloadBtn>
          </ResumeNav>
        </Column>
        <Column lgStart={5} lgEnd={13}>
          <DownloadMobile>
            <DownloadBtn href={downloadResume} download>
              Download resume
            </DownloadBtn>
          </DownloadMobile>
          <H2>Work Experience</H2>
          <ResumeBlock id="work-experience">
            <Title>Senior Digital Designer</Title>
            <Subtitle>Web Designer / Frontend Web Developer</Subtitle>
            <Details>
              <A
                href="https://1valet.com"
                target="_blank"
                rel="noopener noreferrer"
              >
                1VALET
              </A>
              , Canada · November 2021 - <i>July 2022</i>
            </Details>
            <UList>
              <ListItem>
                Developed scalable and reusable component-based headless web
                application with React/Gatsby, GraphQL and DatoCMS
              </ListItem>
              <ListItem>
                Increased landing page conversion rates and reduced sales funnel
                cycle up to 90% through website form optimizations
              </ListItem>
              <ListItem>
                Improved 1valet.com page speed metrics, boosting scores from an
                average of 25/100 to 95/100 on both mobile and desktop
              </ListItem>
              <ListItem>
                Designed and maintained marketing assets (digital ads, landing
                pages, emails) and sales enablement tools (white papers,
                presentation sales decks)
              </ListItem>
            </UList>
          </ResumeBlock>

          <ResumeBlock id="work-experience">
            <Title>Marketing Designer</Title>
            <Subtitle>
              Brand Designer / Web Designer / Frontend Web Developer
            </Subtitle>
            <Details>
              <A
                href="https://boldcommerce.com"
                target="_blank"
                rel="noopener noreferrer"
              >
                Bold Commerce
              </A>
              , Winnipeg, Canada · June 2018 - November 2021
            </Details>
            <UList>
              <ListItem>
                Redesigned and rebuilt boldcommerce.com in 2 months using Jekyll
                static site generator
              </ListItem>
              <ListItem>
                Designed an illustration library of over 100 illustrations and
                icons for rebranding initiatives
              </ListItem>
              <ListItem>
                Delivered a design system for Human Resources’s hiring efforts,
                saving up to 10% of the Creative team’s time per week
              </ListItem>
              <ListItem>
                Championed adherence to brand guidelines across 3 departments
                (Sales, Marketing and Partnerships)
              </ListItem>
              <ListItem>
                Contributed to design culture between Marketing and Product
                Design teams
              </ListItem>
              <ListItem>
                Educated and managed stakeholder expectations around a
                shared-understanding of user-centric design methodologies
              </ListItem>
              <ListItem>
                Strategized solutions for improving website user experience and
                lead generation on boldcommerce.com and blog
              </ListItem>
              <ListItem>
                Supported over 20 people in the Marketing team to translate
                business goals and marketing strategies into lead generating
                pixel-perfect visual graphics and responsive website landing
                pages
              </ListItem>
            </UList>
          </ResumeBlock>

          <ResumeBlock>
            <Title>Founder & Lead Designer</Title>
            <Subtitle>Visual Designer / Manager</Subtitle>

            <Details>
              Goliath Digital Studio, Portugal · February 2012 - December 2017
            </Details>

            <UList>
              <ListItem>
                Produced over 200 design concepts, layouts and high quality
                visual graphics for Prezi presentations
              </ListItem>
              <ListItem>
                Managed and maintained relationships with over 100 US market
                SMB’s, along with multi-national companies such as Cisco;
              </ListItem>
              <ListItem>
                Managed and maintained client relationships with US market
                SMB’s, along with multi-national companies such as Cisco
              </ListItem>
              <ListItem>
                Coordinated a team of 4 designers (freelances and in-house)
              </ListItem>
            </UList>
          </ResumeBlock>

          <ResumeBlock id="education">
            <H2>Education</H2>
            <Title>
              Bachelor's Degree in Multimedia, Education and Communication
            </Title>
            <Details>
              Polytechnic Insitute of Santarém, Portugal · 2008 - 2011
            </Details>
          </ResumeBlock>

          <ResumeBlock id="skills-and-tools">
            <H2>Skills and Tools</H2>
            <ResumeFeaturesWrapper>
              <ResumeFeatures>
                <Title>Design</Title>
                Visual Brand Design, Atomic Design Systems, Illustration,
                Iconography, User Interface Design, User Experience Design,
                Landing Pages, Digital Advertising, Presentation and Email
                Design, Low and High Fidelity Mockups, Accessibility
              </ResumeFeatures>
              <ResumeFeatures>
                <Title>Frontend Development</Title>
                HTML5, CSS3, SASS, Javascript, jQuery, React, Gatsby, Jekyll,
                Liquid, GraphQL, Headless Content Management Systems, Responsive
                Mobile-First Design, Styled-Components, Bootstrap, Git, SEO,
                Lighthouse
              </ResumeFeatures>
              <ResumeFeatures>
                <Title>Tools</Title>
                Slack, Asana, JIRA, Microsoft Office, Adobe Creative Cloud,
                Google Analytics, Figma, Figjam, Wordpress, Hubspot, DatoCMS,
                Sanity, Contentful, Strapi
              </ResumeFeatures>
            </ResumeFeaturesWrapper>
          </ResumeBlock>
        </Column>
      </MaxWidthSection>
    </Layout>
  )
}

export default Resume
