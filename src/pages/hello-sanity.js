import React from "react"
import styled from "styled-components"
import { Helmet } from "react-helmet"

import Layout from "../components/layout"
import SeoData from "../components/SeoData"
import MaxWidthSection from "../components/layout/MaxWidthSection"
import Column from "../components/layout/Column"
import { Link } from "gatsby"

import { H1, H3, P, A } from "../components/elements"

const LargeH1 = styled(H1)`
  font-size: 6rem;
`

const LargeP = styled(P)`
  font-size: 3rem;
  line-height: 120%;
`

const Highlight = styled.span`
  color: ${props => props.theme.colors.primary};
`

const HelloSanity = () => {
  return (
    <Layout>
      <Helmet>
        <meta name={`robots`} content={`noindex, nofollow`} />
      </Helmet>
      <SeoData title="Hello Sanity" />
      <MaxWidthSection as="header">
        <Column>
          <LargeH1>Hello Sanity &#128075;</LargeH1>
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column>
          <LargeP>
            My name is David Oliveira and I'm a Jamstack enthusiast and{" "}
            <Highlight>
              I would love to apply to the role of Lead Web Designer on your
              team!
            </Highlight>
          </LargeP>
          <P>
            I couldn't just send my resume without a chance to introduce myself
            and let you know how I would <b>love</b> to be part of the team that
            is spearheading a new way teams can manage their content on
            websites, apps and products!
          </P>
          <H3 style={{ marginBottom: "2rem" }}>
            Do I even have experience working with Sanity, or with any other
            headless projects?
          </H3>
          <P>
            <em>Absolutely!</em>
          </P>
          <P>
            Last year,{" "}
            <A>
              <Link to="/headless-content-modeling-in-sanity/">
                I built an entire content model with Sanity schema while at Bold
                Commerce
              </Link>
            </A>
            , and have recently kickstarted a project to provide an easy way for
            content creators across multiple teams to generate templated web
            pages using React/Gatsby and fetching content from DatoCMS.
          </P>
          <P>
            I'm a multidisciplinary individual with relevant experience in
            design and front end development. For the past 4 years, I've been
            leading the design, development, and maintenance of the web
            presences at Bold Commerce and 1VALET. I strongly believe my skills
            can have a strong and positive impact on the team.
          </P>
          <P>
            I reside in Winnipeg, Canada,{" "}
            <b>which is only 1 hour behind the timezone in the East Coast</b>. I
            have been working remotely for over 2 years, and I'm 100%
            comfortable collaborating with teams across a different timezone.
            (for 5 years, I worked with US-based clients daily while in
            Portugal, and the time differences would span from -5 hours to -8
            hours)
          </P>
          <P>
            I would love to make time to speak with your team and show my
            excitement for your product and the tools Sanity has been building.
            My email is{" "}
            <A href="mailto:davidalex.oliveira@gmail.com">
              davidalex.oliveira@gmail.com
            </A>{" "}
            and you can find more information{" "}
            <A>
              <Link to="/about">about me</Link>
            </A>
            ,{" "}
            <A>
              <Link to="/work">my work</Link>
            </A>
            , or{" "}
            <A>
              <Link to="/resume">my resume</Link>
            </A>{" "}
            here on my website.
          </P>
          <P>Hope to hear from you soon!</P>
        </Column>
      </MaxWidthSection>
    </Layout>
  )
}

export default HelloSanity
