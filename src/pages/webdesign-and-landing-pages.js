import React from "react"

import { StaticImage } from "gatsby-plugin-image"
import Layout from "../components/layout"
import SeoData from "../components/SeoData"
import { H1, H2, P } from "../components/elements"

import MaxWidthSection from "../components/layout/MaxWidthSection"
import Column from "../components/layout/Column"

import Slider from "../portfolio/webdesign-and-landing-pages/web.mp4"

const WebPages = () => {
  return (
    <Layout>
      <SeoData title="Webdesign and Landing Pages" />
      <MaxWidthSection as="header">
        <Column mdStart={3} mdEnd={13}>
          <H1>Webdesign and Landing Pages</H1>
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <H2>Featured Customer Carousel</H2>
          <P>
            Image carousel design featuring subscription customers. The webpage
            images slide into a browser illustration as a visual metaphor for
            viewing the pages on a browser. Left and right slider buttons sync
            up with the customers logos at the bottom for easy access to any
            customer. Designed in Figma.
          </P>
          <video muted loop autoPlay style={{ width: "100%" }}>
            <source src={Slider} type="video/mp4" />
          </video>
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <H2>Holiday Landing Page</H2>
          <P>
            End-of-year winter themed landing page to promote ecommerce apps
            following BFCM. Built in Jekyll.
          </P>
          <StaticImage
            src="../portfolio/webdesign-and-landing-pages/web-1.png"
            alt="holiday landing page mockup"
            placeholder="blurred"
            layout="constrained"
          />
        </Column>
      </MaxWidthSection>
      <MaxWidthSection>
        <Column mdStart={3} mdEnd={11}>
          <H2>Subscription Trends Landing Page</H2>
          <P>
            Landing page designed for lead generation and offering gated-content
            (PDF Report) post form submission. Campaign included targeted social
            ads to drive traffic to page. Built in Jekyll.
          </P>
          <StaticImage
            style={{ marginBottom: "2rem" }}
            src="../portfolio/webdesign-and-landing-pages/web-3.png"
            alt="subscription trends landing page"
            placeholder="blurred"
            layout="constrained"
          />
          <StaticImage
            src="../portfolio/webdesign-and-landing-pages/web-4.png"
            alt="PDF promotion ad"
            placeholder="blurred"
            layout="constrained"
          />
        </Column>
      </MaxWidthSection>
    </Layout>
  )
}

export default WebPages
