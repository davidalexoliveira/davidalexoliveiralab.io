import React from "react"
import styled from "styled-components"
import VersionTwoFile from "../../portfolio/illustrations/versiontwo.gif"

const GIFWrapper = styled.img`
  max-width: 100%;
  height: auto;
  width: auto;
`

export const VersionTwoGIF = () => {
  return (
    <GIFWrapper
      src={VersionTwoFile}
      alt="Diagram about emotions and behaviours"
    />
  )
}
