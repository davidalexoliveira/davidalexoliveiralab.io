import React from "react"
import { GatsbyImage } from "gatsby-plugin-image"
import { Link } from "gatsby"
import styled from "styled-components"

export const FeatureImageWrapper = styled.a`
  margin-bottom: 1.5rem;
  display: flex;
`

const FeatureImage = ({ fixed, slug }) => {
  return (
    <FeatureImageWrapper as={Link} to={slug}>
      <GatsbyImage fixed={fixed} />
    </FeatureImageWrapper>
  )
}

export default FeatureImage
