import React from "react"
import styled from "styled-components"
import { Link } from "gatsby"

const ProjectCol = styled.div`
  margin-bottom: 2rem;

  :nth-child(1) a {
    background-color: #97fffa;
  }

  :nth-child(2) a {
    background-color: #ffe46f;
  }

  :nth-child(3) a {
    background-color: #bec0ff;
  }

  :nth-child(4) a {
    background-color: #ffbebe;
  }
`

const ProjectWrapper = styled.a`
  display: block;
  padding: 1rem 2rem;
  margin-bottom: 2rem;
  text-decoration: none;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  transition: transform 0.2s ease-in;

  &:hover {
    transform: translateY(-8px);
  }
`

const ProjectTitle = styled.h2`
  margin-bottom: 1rem;
  font-size: 2rem;
`

const ProjectDescription = styled.h4`
  margin-bottom: 0;
  font-weight: 400;
`

const Projectold = ({ title, description, slug }) => {
  return (
    <oldCol className="col-md-6">
      <ProjectWrapper as={Link} to={slug}>
        <ProjectTitle>{title} →</ProjectTitle>
        <ProjectDescription>{description}</ProjectDescription>
      </ProjectWrapper>
    </oldCol>
  )
}

export default Projectold
