import React from "react"
import styled from "styled-components"
import CopaLagosCupFile from "../../portfolio/photomanipulation/copalagos-cup.gif"

const GIFWrapper = styled.img`
  max-width: 100%;
  height: auto;
  width: auto;
`

export const CopaLagosCupGIF = () => {
  return <GIFWrapper src={CopaLagosCupFile} alt="Custom Pepsi design on cup" />
}
