import React from "react"
import styled from "styled-components"
import { StaticImage } from "gatsby-plugin-image"
import { Link } from "gatsby"

const LogoWrapper = styled.div`
  display: block;
  height: 80px;
  width: 80px;
  overflow: hidden;
  border-radius: 50%;
  background: ${props => props.theme.colors.gradient};

  .gatsby-image-wrapper {
    transition: transform 0.2s ease-in-out !important;
  }

  :hover .gatsby-image-wrapper {
    transform: scale(1.1);
  }
`
const Logo = () => {
  return (
    <LogoWrapper as={Link} to="/">
      <StaticImage
        src="../images/photo-logo.png"
        alt="David Oliveira"
        placeholder="blurred"
        layout="fixed"
        width={80}
        height={80}
      />
    </LogoWrapper>
  )
}

export default Logo
