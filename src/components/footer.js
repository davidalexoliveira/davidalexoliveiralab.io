import React from "react"
import styled from "styled-components"

import MaxWidthSection from "./layout/MaxWidthSection"
import Column from "./layout/Column"

const FooterWrapper = styled(MaxWidthSection)`
  color: #657181;
  text-align: center;
  font-size: 1rem;
  padding: 1rem 0;

  @media ${props => props.theme.breakpoints.md} {
    padding: 4rem 0;
  }
`

const Copyright = styled.div`
  margin-top: 0.5rem;
  font-style: italic;
  text-align: center;
`

const BuildTag = styled.span`
  margin-top: 0.5rem;
  font-style: italic;
  text-align: center;
`

const Footer = () => {
  let year = new Date().getFullYear()

  return (
    <FooterWrapper as="footer">
      <Column>
        <BuildTag>Made with React and Gatsby</BuildTag>
        <Copyright>©{year} David Oliveira</Copyright>
      </Column>
    </FooterWrapper>
  )
}

export default Footer
