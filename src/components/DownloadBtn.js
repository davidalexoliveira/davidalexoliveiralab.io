import React from "react"
import styled from "styled-components"

const SvgElement = () => {
  return (
    <svg
      width="14"
      height="19"
      viewBox="0 0 14 19"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M6.2395 13.9896L6.2395 0L7.7395 -6.56808e-08L7.7395 13.9896L6.2395 13.9896Z" />
      <path d="M0.469482 8.52417L1.53054 7.4639L7.00429 12.9336L12.4686 7.45443L13.5315 8.51286L7.00613 15.056L0.469482 8.52417Z" />
      <path d="M1 17.25H13V18.75H1V17.25Z" />
    </svg>
  )
}

const DownloadWrapper = styled.a`
  text-decoration: none;
  display: flex;
  align-items: center;
  padding: 1rem;
  border-radius: 2px;
  background-color: ${props => props.theme.colors.primary};
  width: fit-content;
  color: #fff;
  cursor: pointer;
  margin-top: 1rem;

  svg {
    fill: #fff;
  }

  :hover {
    background-color: #000;
  }
`

const IconWrapper = styled.div`
  height: 24px;
  width: 24px;
  margin-left: 6px;
  margin-top: 2px;
  display: flex;
  align-items: center;
  justify-content: center;
`

const DownloadBtn = ({ href, children }) => {
  return (
    <DownloadWrapper href={href} target="_blank">
      {children}
      <IconWrapper>
        <SvgElement />
      </IconWrapper>
    </DownloadWrapper>
  )
}

export default DownloadBtn
