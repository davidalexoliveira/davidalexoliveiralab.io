import styled from "styled-components"

const NavLinksWrapper = styled.ul`
  text-align: right;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  justify-content: flex-end;
  padding-top: 0.5rem;

  @media ${props => props.theme.breakpoints.md} {
    flex-direction: row;
    align-items: center;
    transition: all 0.3s ease-in;
  }

  ul {
    background-color: transparent;
    position: relative;
  }
`

export default NavLinksWrapper
