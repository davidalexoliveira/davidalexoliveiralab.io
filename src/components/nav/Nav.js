import React from "react"
import styled from "styled-components"
import { Link } from "gatsby"
import NavLinks from "./NavLinks"
import NavLinksWrapper from "./NavLinksWrapper"

import Logo from "../Logo"

import MaxWidthSection from "../layout/MaxWidthSection"
import Column from "../layout/Column"

const NavWrapper = styled(MaxWidthSection)`
  padding-top: 1rem;
`

const FlexWrapper = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: space-between;

  @media ${props => props.theme.breakpoints.md} {
    align-items: center;
  }
`

const Nav = () => {
  return (
    <NavWrapper as="nav">
      <Column>
        <FlexWrapper>
          <Logo />
          <NavLinksWrapper>
            <NavLinks>
              <Link to="/work" activeClassName="active">
                Work
              </Link>
            </NavLinks>
            <NavLinks>
              <Link to="/about" activeClassName="active">
                About
              </Link>
            </NavLinks>
            <NavLinks>
              <Link to="/resume" activeClassName="active">
                Resume
              </Link>
            </NavLinks>
          </NavLinksWrapper>
        </FlexWrapper>
      </Column>
    </NavWrapper>
  )
}

export default Nav
