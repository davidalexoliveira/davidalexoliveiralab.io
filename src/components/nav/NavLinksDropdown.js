import styled from "styled-components"

const NavLinksWrapper = styled.div`
  background: #fff;
  width: 100%;
  justify-content: center;
  height: 100%;
  display: flex;
  align-items: center;
  top: 0;
  flex-direction: column;
  position: fixed;
  z-index: 1020;
  transition: all 0.3s ease-in;
  left: ${props => (props.open ? "-100%" : "0")};

  @media ${props => props.theme.breakpoints.md} {
    display: flex;
    flex-direction: row;
    position: unset;
    justify-content: flex-end;
    align-items: center;
    height: auto;
    width: auto;
  }
`

export default NavLinksWrapper
