import React from "react"
import styled from "styled-components"

const NavItem = styled.li`
  font-size: 1.25rem;
  position: relative;
  margin-bottom: 0.25rem;
  text-align: left;

  :last-child {
    margin-bottom: 0;
  }

  @media ${props => props.theme.breakpoints.md} {
    white-space: nowrap;
    margin-right: 2rem;
    margin-bottom: 0;

    :last-child {
      margin-right: 0;
    }
  }

  a,
  button {
    text-decoration: none;
  }

  a {
    ::before,
    ::after {
      content: "";
      position: absolute;
      bottom: 0;
      left: 0;
      right: 0;
      height: 2px;
      background: ${props => props.theme.colors.primary};
    }
    ::before {
      opacity: 0;
      transform: translateY(-8px);
      transition: transform 0s cubic-bezier(0.175, 0.885, 0.32, 1.275),
        opacity 0s;
    }
    ::after {
      opacity: 0;
      transform: translateY(8px/2);
      transition: transform 0.2s cubic-bezier(0.175, 0.885, 0.32, 1.275),
        opacity 0.2s;
    }
    :hover,
    :focus {
      ::before,
      ::after {
        opacity: 1;
        transform: translateY(0);
      }
      ::before {
        transition: transform 0.2s cubic-bezier(0.175, 0.885, 0.32, 1.275),
          opacity 0.2s;
      }
      ::after {
        transition: transform 0s 0.2s cubic-bezier(0.175, 0.885, 0.32, 1.275),
          opacity 0s 0.2s;
      }
    }
  }

  a.active {
    ::before,
    ::after {
      content: "";
      position: absolute;
      bottom: 0;
      left: 0;
      right: 0;
      height: 2px;
      background-color: ${props => props.theme.colors.primary};
      opacity: 1;
      transform: translateY(0);
    }
  }
`

const NavLinks = ({ children }) => {
  return <NavItem>{children}</NavItem>
}

export default NavLinks
