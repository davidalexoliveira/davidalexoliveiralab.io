import React from "react"
import FullWidthSection from "./layout/FullWidthSection"

const Header = ({ children }) => (
  <FullWidthSection as="header">{children}</FullWidthSection>
)

export default Header
