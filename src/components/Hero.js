import React from "react"
import { StaticImage } from "gatsby-plugin-image"

import styled from "styled-components"

import { H1 } from "./elements"
import Logo from "./Logo"
import FullWidthSection from "./layout/FullWidthSection"
import Column from "./layout/Column"
import { Link } from "gatsby"
import NavLinks from "./nav/NavLinks"
import NavLinksWrapper from "./nav/NavLinksWrapper"

const ContentWrapper = styled.div`
  padding: 60px 0;

  @keyframes outer-left {
    from {
      left: 50%;
      transform: translate(-50%, -80%);
    }
    to {
      left: 0;
      transform: translate(0, -80%);
    }
  }

  @keyframes navfade {
    0% {
      opacity: 0;
      transform: translateY(-2px);
    }
    100% {
      opacity: 1;
      transform: translateY(0);
    }
  }

  @keyframes fadein {
    0% {
      opacity: 0;
    }
    30% {
      opacity: 1;
    }
    100% {
      opacity: 1;
    }
  }

  @keyframes reveal {
    0% {
      opacity: 0;
      width: 0px;
    }
    30% {
      opacity: 0;
      width: 0px;
    }
    60% {
      opacity: 1;
      width: 370px;
    }
    100% {
      opacity: 1;
      width: 370px;
    }
  }

  @keyframes slidein {
    0% {
      margin-left: -370px;
    }
    30% {
      margin-left: -370px;
    }
    60% {
      margin-left: 0px;
    }
    100% {
      margin-left: 0px;
    }
  }

  @keyframes slideup {
    0% {
      opacity: 0;
      transform: translateY(-10px);
    }
    100% {
      opacity: 1;
      transform: translateY(0);
    }
  }

  @keyframes scaledown {
    0% {
      transform: scale(1.02);
    }
    100% {
      opacity: 1;
      transform: scale(1);
    }
  }

  @media ${props => props.theme.breakpoints.md} {
    height: 100vh;
    display: flex;
    flex-direction: column;
    justify-content: center;
  }
`

const LogoWrapper = styled.div`
  display: block;
  margin-bottom: 2rem;
  animation: navfade 0.5s ease-in both;

  @media ${props => props.theme.breakpoints.md} {
    display: none;
  }
`

const HeroTitle = styled(H1)`
  position: relative;
  word-break: break-word;
  margin-bottom: 2rem;

  @media ${props => props.theme.breakpoints.md} {
    font-size: 5rem;
  }
`

const Opening = styled.span`
  display: inline-flex;
  font-size: 3rem;
  margin-bottom: 1rem;
  animation: navfade 0.5s ease-in both;
  animation-delay: 0.3s;

  @media ${props => props.theme.breakpoints.md} {
    font-size: 5rem;
    margin-bottom: 0;
    width: max-content;
  }

  @media ${props => props.theme.breakpoints.md} {
    position: absolute;
    display: inline-block;
    animation: outer-left 1s 1s ease both;
    animation-delay: 2s;
  }
`
const Hi = styled.span`
  display: inline-block;
  overflow: hidden;
  white-space: nowrap;
  margin-right: 0.5rem;
  @media ${props => props.theme.breakpoints.md} {
    margin-right: 1rem;
    animation: fadein 2s ease-in forwards;
  }
`

const NameReveal = styled.span`
  display: inline-block;
  overflow: hidden;
  white-space: nowrap;
  @media ${props => props.theme.breakpoints.md} {
    animation: reveal 3s ease-in-out forwards;

    span {
      animation: slidein 3s ease-in-out;
    }
  }
`

const Name = styled.span`
  display: block;
`

const Intro = styled.span`
  font-family: Open Sans,Arial,Helvetica,sans-serif;
  text-align left;
  display: block;
  animation: navfade 0.5s ease-in both;
  animation-delay: 0.6s;
  font-size: 2rem;
  line-height: 120%;

  @media ${props => props.theme.breakpoints.md} {
    font-size: 2rem;
    padding-right: 15rem;
    opacity: 0;
    animation: slideup 0.7s ease-in forwards;
    line-height: 150%;
    animation-delay: 2.5s;
    max-width: 600px;
  }

  @media ${props => props.theme.breakpoints.lg} {
    padding-right: 0;
  }
`

const HeroNav = styled(NavLinksWrapper)`
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;

  li {
    font-size: 1.5rem;
    margin-bottom: 1rem;
    opacity: 0;
    animation: navfade 0.5s ease-in forwards;
  }

  > :nth-child(1) {
    animation-delay: 0.9s;
  }
  > :nth-child(2) {
    animation-delay: 1.2s;
  }
  > :nth-child(3) {
    animation-delay: 1.5s;
  }

  @media ${props => props.theme.breakpoints.md} {
    font-size: 4rem;

    li {
      margin-bottom: 0;
      animation: navfade 0.5s ease-in forwards;
    }

    > :nth-child(1) {
      animation-delay: 3.5s;
    }
    > :nth-child(2) {
      animation-delay: 3.7s;
    }
    > :nth-child(3) {
      animation-delay: 3.9s;
    }
  }
`

const BackgroundImage = styled.div`
  display: none;
  align-items: flex-end;
  justify-content: flex-end;
  height: 100vh;
  overflow: hidden;
  position: relative;

  ::before {
    content: "";
    background: ${props => props.theme.colors.gradient};
    height: 800px;
    width: 800px;
    position: absolute;
    bottom: 0;
    right: -350px;
    display: block;
    border-radius: 50%;
    opacity: 0;
    animation: fadein 0.5s ease-in forwards;
    animation-delay: 3.25s;
  }

  .gatsby-image-wrapper {
    margin-right: -16rem;
    opacity: 0;
    animation: scaledown 0.8s ease-in forwards;
    animation-delay: 2.75s;
    height: auto;
    max-height: 100vh;
  }

  @media ${props => props.theme.breakpoints.md} {
    display: flex;

    .gatsby-image-wrapper {
      margin-right: -22rem;
    }
  }

  @media ${props => props.theme.breakpoints.lg} {
    ::before {
      right: -220px;
    }

    .gatsby-image-wrapper {
      margin-right: -16rem;
    }
  }

  @media ${props => props.theme.breakpoints.xl} {
    ::before {
      right: -10px;
    }

    .gatsby-image-wrapper {
      margin-right: -3rem;
    }
  }
  @media ${props => props.theme.breakpoints.xxl} {
    ::before {
      right: 150px;
    }

    .gatsby-image-wrapper {
      margin-right: 7rem;
    }
  }
`

const Hero = () => {
  return (
    <>
      <FullWidthSection
        as="div"
        style={{
          gridArea: "1 / 1 / auto / span 14",
        }}
      >
        <Column>
          <BackgroundImage>
            <StaticImage
              src="../images/hero-image.png"
              alt="David Oliveira"
              placeholder="blurred"
              layout="constrained"
              aspectRatio={1 / 1}
            />
          </BackgroundImage>
        </Column>
      </FullWidthSection>
      <FullWidthSection
        as="header"
        style={{
          gridArea: "1 / 1 / auto / span 14",
        }}
      >
        <Column smStart={2} smEnd={14}>
          <ContentWrapper>
            <LogoWrapper>
              <Logo />
            </LogoWrapper>
            <HeroTitle>
              <Opening>
                <Hi>Hi!</Hi>
                <NameReveal>
                  <Name>I'm David!</Name>
                </NameReveal>
              </Opening>
              <Intro>
                I'm a front-end web designer & developer from Canada.
              </Intro>
            </HeroTitle>
            <HeroNav>
              <NavLinks>
                <Link to="/work">Work</Link>
              </NavLinks>
              <NavLinks>
                <Link to="/about">About</Link>
              </NavLinks>
              <NavLinks>
                <Link to="/resume">Resume</Link>
              </NavLinks>
            </HeroNav>
          </ContentWrapper>
        </Column>
      </FullWidthSection>
    </>
  )
}

export default Hero
