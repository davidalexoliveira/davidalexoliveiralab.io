import styled, { css } from "styled-components"

const maxWidth = () => {
  return css`
    @media ${props => props.theme.grid.queries.sm} {
      max-width: ${props => props.theme.grid.breakpoints.sm};
    }

    @media ${props => props.theme.grid.queries.md} {
      max-width: ${props => props.theme.grid.breakpoints.md};
    }

    @media ${props => props.theme.grid.queries.lg} {
      max-width: ${props => props.theme.grid.breakpoints.lg};
    }

    @media ${props => props.theme.grid.queries.xl} {
      max-width: ${props => props.theme.grid.breakpoints.xl};
    }

    @media ${props => props.theme.grid.queries.xxl} {
      max-width: ${props => props.theme.grid.breakpoints.xxl};
    }
  `
}

export const Container = styled.div`
  margin: 0 auto;
  padding-right: ${props => props.theme.grid.gutter};
  padding-left: ${props => props.theme.grid.gutter};
  width: 100%;
  margin-right: auto;
  margin-left: auto;
  ${props => (props.fluid ? null : maxWidth())}
`
