import styled, { css } from "styled-components"

const mediaQuery = props => {
  const themeBreakpoints = Object.keys(props.theme.grid.breakpoints).map(
    function (themeKey) {
      return themeKey
    }
  )

  return Object.entries(props).map(function ([key, value]) {
    const propKey = key
    const propValue = value

    console.log(key, value)

    return function getBreakpointCSS() {
      if (themeBreakpoints.includes(propKey)) {
        return css`
          @media ${props => props.theme.grid.queries[propKey]} {
            width: ${props => props.theme.grid.widths[propValue]};
          }
        `
      }
    }
  })
}

export const Column = styled.div`
  margin: 0 auto;
  width: ${props => (props.xs ? props.theme.grid.widths[props.xs] : "100%")};
  max-width: 100%;
  flex: 0 0 auto;
  padding-right: ${props => props.theme.grid.gutter};
  padding-left: ${props => props.theme.grid.gutter};

  ${props => mediaQuery(props)};
`
