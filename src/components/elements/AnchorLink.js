import React from "react"
import styled from "styled-components"

const Anchor = styled.a`
  text-decoration: none;
  padding: 1rem;

  :hover {
    color: ${props => props.theme.colors.primary};
  }
`

export const AnchorLink = ({ children, href, title }) => {
  return (
    <Anchor href={href} title={title}>
      {children}
    </Anchor>
  )
}
