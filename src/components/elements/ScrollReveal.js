import React from "react"
import styled from "styled-components"

const ScrollRevealWrapper = styled.div``

export const ScrollRevealSlideLeft = ({ children }) => {
  return (
    <ScrollRevealWrapper
      data-sal="slide-left"
      data-sal-delay="500"
      data-sal-easing="ease-in"
    >
      {children}
    </ScrollRevealWrapper>
  )
}

export const ScrollRevealFadeIn = ({ children }) => {
  return (
    <ScrollRevealWrapper
      data-sal="fade-in"
      data-sal-delay="300"
      data-sal-easing="ease"
    >
      {children}
    </ScrollRevealWrapper>
  )
}
