import styled from "styled-components"

const SectionWrapper = styled.section`
  padding: 2rem 0;
`

export default SectionWrapper
