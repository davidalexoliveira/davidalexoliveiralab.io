// import React from "react";
// import styled from "styled-components";
// import { graphql } from "gatsby";

// import { Section, Container, Row, Column } from "../layout";

// const Header = styled(Section)``;

// const TwoColumnHeaderGeneralContent = (header) => {
//   const name = header.value.document.children[1].children[0].value;
//   return (
//     <Header as="header">
//       <Container>
//         <Row>
//           <Column>
//             <h1>{name}</h1>
//           </Column>
//         </Row>
//       </Container>
//     </Header>
//   );
// };

// export const query = graphql`
//   fragment TwoColumnHeaderGeneralContentFragment on DatoCmsBuildingBrochure {
//     header {
//       id
//       __typename
//       content {
//         value
//         links {
//           ... on DatoCmsPost {
//             id
//             __typename
//           }
//           ... on DatoCmsPage {
//             id
//           }
//         }
//       }
//     }
//   }
// `;

// export default TwoColumnHeaderGeneralContent;
