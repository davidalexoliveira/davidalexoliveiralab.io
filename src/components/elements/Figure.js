import React from "react"
import styled from "styled-components"

export const FigureWrapper = styled.figure`
  margin-bottom: 2rem;

  .gatsby-image-wrapper {
    margin-bottom: 0 !important;
  }

  figcaption {
    text-align: center;
    font-size: 0.875rem;
    padding: 0.5rem 0;
    font-style: italic;
  }
`

const Figure = ({ children, figcaption }) => {
  return (
    <FigureWrapper>
      {children}
      <figcaption>{figcaption}</figcaption>
    </FigureWrapper>
  )
}

export default Figure
