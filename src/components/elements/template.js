// import React from "react"
// import { graphql } from "gatsby"

// import {
//   HeaderSwitcher,
//   SectionSwitcher,
// } from "../components/helpers/SwitchComponents"

// const PageTemplate = ({ data }) => {
//   const pages = data.allDatoCmsBuildingBrochure.edges[0].node
//   const header = pages.header[0]
//   const sections = pages.sections

//   const HeaderComponent = HeaderSwitcher[header.__typename]
//   return (
//     <>
//       <HeaderComponent key={header.id} {...header.content} />
//       {sections.map(section => {
//         const SectionComponent = SectionSwitcher[section.__typename]
//         return <SectionComponent key={section.id} {...header.content} />
//       })}
//     </>
//   )
// }

// export const query = graphql`
//   query MyQuery($id: String!) {
//     allDatoCmsBuildingBrochure(filter: { slug: { eq: $id } }) {
//       edges {
//         node {
//           id
//           ...TwoColumnHeaderGeneralContentFragment
//           sections {
//             ... on DatoCmsTwoColumnSectionGeneralContent {
//               id
//               __typename
//             }
//             ... on DatoCmsTwoColumnSectionFeatureHighlight {
//               id
//               __typename
//             }
//             ... on DatoCmsOneColumnSectionFeatureCardsGrid {
//               id
//               __typename
//             }
//             ... on DatoCmsTwoColumnSectionContentAndStepByStepProcess {
//               id
//               __typename
//             }
//           }
//         }
//       }
//     }
//   }
// `

// export default PageTemplate
