import styled from "styled-components"

export const Card = styled.div`
  background-color: #fff;
  box-shadow: 0 0px 16px 0 rgba(0, 0, 0, 0.1);
  height: auto;

  svg {
    width: 100%;
    height: auto;
  }
`
