import React from "react"
import styled from "styled-components"
import { Link } from "gatsby"

const ButtonWrapper = styled(Link)`
  text-decoration: none;
  display: flex;
  align-items: center;
  padding: 0.5rem 1rem;
  font-size: 1rem;
  border-radius: 2px;
  background-color: ${props => props.theme.colors.primary};
  width: fit-content;
  color: #fff;
  cursor: pointer;

  :hover {
    background-color: #000;
  }
`

const Button = ({ href, children }) => {
  return <ButtonWrapper to={href}>{children}</ButtonWrapper>
}

export default Button
