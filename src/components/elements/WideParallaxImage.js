import React from "react"
import styled from "styled-components"

const WideParallaxImageWrapper = styled.div`
  position: relative;
  width: 100%;
  overflow: hidden;
  max-height: 500px;

  img {
    transform: scale(1.5);
  }
`
export const WideParallaxImage = ({ children }) => {
  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-12">
          <WideParallaxImageWrapper>{children}</WideParallaxImageWrapper>
        </div>
      </div>
    </div>
  )
}
