import styled from "styled-components"

export const H1 = styled.h1`
  font-size: 2.5rem;
  line-height: 120%;

  @media ${props => props.theme.breakpoints.md} {
    font-size: 3rem;
  }
`

export const H2 = styled.h2`
  font-size: 1.5rem;
  line-height: 120%;
  margin-bottom: 0.5rem;

  @media ${props => props.theme.breakpoints.md} {
    margin-bottom: 1.5rem;
    font-size: 2rem;
  }
`

export const H3 = styled.h3`
  font-size: 1.5rem;
  margin-bottom: 1rem;
`

export const UList = styled.ul`
  padding-left: 3rem;
  margin-bottom: 2rem;
`

export const ListItem = styled.li`
  margin-bottom: 0.5rem;
  position: relative;
  font-size: 1.125rem;
  line-height: 150%;

  ::before {
    content: "—";
    position: absolute;
    top: -2px;
    left: -24px;
  }
`

export const P = styled.p`
  margin-bottom: 2rem;
`

export const B = styled.b`
  font-weight: bolder;
`

export const A = styled.a`
  text-decoration: underline;

  :hover {
    color: ${props => props.theme.colors.primary};
  }
`
