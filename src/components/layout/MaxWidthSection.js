import styled from "styled-components"

const MaxWidthSection = styled.section`
  display: grid;
  grid-template-rows: max-content;
  grid-column: 2 / 14;
  grid-template-columns: repeat(12, 1fr);
  gap: 1rem 1rem;

  @media ${props => props.theme.breakpoints.md} {
    grid-template-columns: repeat(12, minmax(auto, 4.2rem));
    gap: 4rem 2rem;
  }
`

export default MaxWidthSection
