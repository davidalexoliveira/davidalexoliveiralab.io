import React from "react"
import { StaticImage } from "gatsby-plugin-image"
import styled from "styled-components"

import { Link } from "gatsby"

import { H2, P } from "../elements"
import FullWidthSection from "./FullWidthSection"
import Column from "./Column"

const ListingProjectWrapper = styled(FullWidthSection)`
  padding: 60px 0;

  :hover {
    background-color: #efefef;
    cursor: pointer;

    .gatsby-image-wrapper {
      transform: scale(1.02);
    }

    h2 {
      color: #3185fc;
    }
  }
  .gatsby-image-wrapper {
    transition: transform 0.8s ease-in-out;
  }
`

const ImageColumn = styled(Column)`
  overflow: hidden;
`

const ContentColumn = styled(Column)`
  display: flex;
  flex-direction: column;
  justify-content: center;

  @media ${props => props.theme.breakpoints.md} {
    align-items: flex-start;
  }
`

const ListingProject = ({ image, title, description, href }) => {
  return (
    <ListingProjectWrapper as={Link} href={href}>
      <ImageColumn mdStart={1} mdEnd={6} lgStart={1} lgEnd={10}>
        <StaticImage
          src="../../portfolio/headless-content-modeling-in-sanity/feature-image.png"
          alt="David Oliveira"
          placeholder="blurred"
          layout="constrained"
        />
      </ImageColumn>
      <ContentColumn
        smStart={2}
        smEnd={8}
        mdStart={6}
        mdEnd={8}
        lgStart={11}
        lgEnd={14}
      >
        <H2>{title}</H2>
        <P>{description}</P>
      </ContentColumn>
    </ListingProjectWrapper>
  )
}

export default ListingProject
