import styled from "styled-components"

const FullWidthSection = styled.section`
  display: grid;
  grid-template-rows: max-content;
  grid-column: 1 / -1;
  grid-template-columns: 1rem repeat(12, 1fr) 1rem;
  gap: 4rem 1rem;

  @media ${props => props.theme.breakpoints.md} {
    grid-template-columns: 1fr repeat(12, minmax(auto, 4.2rem)) 1fr;
    gap: 4rem 2rem;
  }
`

export default FullWidthSection
