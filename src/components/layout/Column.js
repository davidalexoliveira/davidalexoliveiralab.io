import styled from "styled-components"

const Column = styled.div`
  grid-column-start: ${props => (props.smStart ? props.smStart : "1")};
  grid-column-end: ${props => (props.smEnd ? props.smEnd : "-1")};

  @media ${props => props.theme.breakpoints.md} {
    grid-column-start: ${props =>
      props.mdStart ? props.mdStart : props.smStart};
    grid-column-end: ${props => (props.mdEnd ? props.mdEnd : props.smEnd)};
  }

  @media ${props => props.theme.breakpoints.lg} {
    grid-column-start: ${props =>
      props.lgStart ? props.lgStart : props.mdStart};
    grid-column-end: ${props => (props.lgEnd ? props.lgEnd : props.mdEnd)};
  }
`

export default Column
