import styled from "styled-components"
import FullWidthSection from "./FullWidthSection"

const DisplayCase = styled(FullWidthSection)`
  background: rgba(0, 0, 0, 0.03);
  text-align: center;
  padding: 4rem;

  .gatsby-image-wrapper {
    margin-bottom: 2rem;
  }
`

export default DisplayCase
