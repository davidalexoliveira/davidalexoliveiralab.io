import React from "react"
import { Helmet } from "react-helmet"

import Grid from "./layout/Grid"
import FullWidthSection from "./layout/FullWidthSection"

import Nav from "./nav/Nav"
import Footer from "./footer"

const Layout = ({ children, noNav, noFooter }) => {
  return (
    <Grid>
      <Helmet>
        <script src="/redirectUrl.js" type="text/javascript" />
      </Helmet>
      {noNav ? null : <Nav />}
      <FullWidthSection as="main">{children}</FullWidthSection>
      {noFooter ? null : <Footer />}
    </Grid>
  )
}

export default Layout
