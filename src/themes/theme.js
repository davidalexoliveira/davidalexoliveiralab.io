const Theme = {
  colors: {
    body: "#0a254a",
    primary: "#3185FC",
    lightBlue: "#A6C9E5",
    darkBlue: "#7D9AC5",
    grey: "#777",
    gradient: "linear-gradient(90deg, #97fffa, #bec0ff)",
  },
  breakpoints: {
    md: "only screen and (min-width: 768px)",
    lg: "only screen and (min-width: 992px)",
    xl: "only screen and (min-width: 1200px)",
    xxl: "only screen and (min-width: 1440px)",
  },
}

export default Theme
