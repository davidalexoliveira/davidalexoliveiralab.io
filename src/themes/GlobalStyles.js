import { createGlobalStyle } from "styled-components"

const GlobalStyles = createGlobalStyle`

html {
  font-size: 100%;
  scroll-behavior: smooth;
}

body {
  font-family: Open Sans, Arial, Helvetica, sans-serif;
  font-size: 1.25rem;
  line-height: 170%;
  font-weight: 400;
  color: ${props => props.theme.colors.body};
  background-color: #fff;
  background-image: linear-gradient(90deg,#f8ffff,#f1f1fd);
}

h1, h2, h3, h4, h5, h6 {
  font-family: Crete Round, Arial, Helvetica, sans-serif;
  font-weight: 400;
  word-break: break-word;
}
`

export default GlobalStyles
