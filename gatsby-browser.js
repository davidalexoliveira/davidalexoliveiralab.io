import React from "react"
import { ThemeProvider } from "styled-components"
import { MDXProvider } from "@mdx-js/react"

import "destyle.css/destyle.css"

import Theme from "./src/themes/theme"
import GlobalStyles from "./src/themes/GlobalStyles"

require("prismjs/themes/prism-tomorrow.css")

const components = {}

export const wrapRootElement = ({ element }) => (
  <MDXProvider components={components}>
    <ThemeProvider theme={Theme}>
      <GlobalStyles />
      {element}
    </ThemeProvider>
  </MDXProvider>
)
