function replacePath(path, old_d, new_d) {
  path = path.replace(old_d, new_d)
  if (path.includes(new_d)) {
    // only if really on the new domain
    path = path.replace("http:", "https:")
  }
  return path
}
newpath = replacePath(
  window.location.href,
  "://davidalexoliveira.gitlab.io",
  "://www.davidoliveira.ca"
)

// Prevent infinite redirect
if (window.location.href != newpath) {
  window.location.replace(newpath)
}
